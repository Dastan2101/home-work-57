
const tasks = [

    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},

];

    const frontEndArray = [];
    const typeBugArray = [];
    const findWord = [];
    let object = {
        Frontend: 0,
        Backend: 0,
    };
    const tasksArray = [];



    tasks.map((task => {
        if (task.category === 'Frontend') {
            frontEndArray.push(task.timeSpent)
            object.Frontend++
        }
        if (task.type === 'bug') {
            typeBugArray.push(task.timeSpent)
        }
        if (task.title.indexOf('UI') !== -1) {
            findWord.push(task)
        }
        if (task.category === 'Backend') {
            object.Backend++
        }
        if (task.timeSpent > 4) {
           let tasksObject = {
                title: task.title,
                category: task.category,
            };
            tasksArray.push(tasksObject)
        }


    }));

   const UIItems = tasks.filter(item => item.title.includes("UI"));


console.log('Frontend timeSpent = ' + frontEndArray.reduce((acc, currentValue) => acc + currentValue));
console.log('Type bug timeSpent = ' + typeBugArray.reduce((acc, currentValue) => acc + currentValue));
console.log('Number of tasks with "UI". = ' + UIItems.length);
console.log(object);
console.log(tasksArray);

    


import React from 'react';
import './FormStyles.css'
const MainForm = props => {
    return(
        <div className="block-form">
            <input type="text" id="text" placeholder="Item name" value={props.title} onChange={props.onChangeTitle}/>
            <input type="number" id="cost" placeholder="Cost" value={props.cost} onChange={props.onChangeCost}/>
            <button id="add-btn" onClick={props.addPurchase}>Add</button>
        </div>
    )
};

export default MainForm;
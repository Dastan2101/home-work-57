import React from 'react';
import './PurchasesStyles.css';

const Purchase = props => {
    return(
        props.array.map((object, key) => {
            return(
                <div className="purchase-block" key={key}>
                    <h3 className="title">{object.title}</h3>
                    <h3 className="cost">{object.cost}</h3>
                    <button onClick={() => props.deletePurchase(key)}>X</button>
                </div>
            )
        })
    )
};

export default Purchase;
import React, {Component} from 'react';
import './App.css';
import MainForm from '../components/MainForm/AddMainForm'
import Purchase from "../components/Purchases/Purchase";

class App extends Component {

    state = {
        purchases: [],
        title: '',
        cost: 0,
        total: 0,
    };


    setTitle = (event) => {
        this.setState({title: event.target.value})

    };

    setCost = (event) => {
        if (event.target.value > 0) {
            this.setState({cost: event.target.value})
        }
         else {
            alert('Cost must be greater than 0');
            event.target.value = '';
        }
    };

    addPurchase = () => {

        const copyState = [...this.state.purchases];

        let object = {
            title: this.state.title,
            cost: this.state.cost
        };

            copyState.push(object);

            let total = copyState.reduce((totalCost, item) => totalCost + parseInt(item.cost), 0);

            this.setState({purchases: copyState, total: total});


    };

    deletePurchase = (index) => {
        const copyState = [...this.state.purchases];

        const indexOfState = copyState[index];
        copyState.splice(index, 1);

        let total = this.state.total;
        total -= indexOfState.cost;

        this.setState({purchases: copyState, total: total})
    };
    render() {
        return (
            <div className="App">
                <div className="purchases-block">
                <MainForm onChangeTitle={(event) => this.setTitle(event)}
                          onChangeCost={(event) => this.setCost(event)}
                          addPurchase={() => this.addPurchase()}
                />
                <Purchase array={this.state.purchases}
                          deletePurchase={this.deletePurchase}
                />
                 <div className="total-spent"><p>Total spent</p>
                 <p>{this.state.total} KGS</p></div>
            </div>
            </div>
        );
    }
}

export default App;
